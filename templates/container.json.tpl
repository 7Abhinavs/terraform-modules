[
  {
    "essential": true,
    "memory": 256,
    "name": "myapp",
    "cpu": 256,
    "image": "${REPOSITORY_URL}:1",
    "workingDirectory": "/app",
    "command": ["npm", "start"],
    "portMappings": [
        {
            "containerPort": 3000,
            "hostPort": 3000
        }
    ],
    "environment": [
     {
      "name" : "username",
      "value" : "user"
     },
     {
      "name" : "password",
      "value" : "abhinav6"
     }
    ]
  }
]