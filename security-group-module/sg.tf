# app
data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../vpc-module/terraform.tfstate"
  }
} 


module "ecs-sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = var.ecs-sg-name
  description = "Security group for ECS service"
  vpc_id      = local.vpc_id

  ingress_cidr_blocks      = var.ingress_cidr_blocks
  ingress_rules            = var.ingress_rules 
  ingress_with_cidr_blocks = [
    {
      from_port   = 8080
      to_port     = 8090
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "10.10.0.0/16"
    },
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}