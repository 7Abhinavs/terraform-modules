variable "ecs-sg-name" {
  type = string
  default = "Ecs-sg"
}

variable "ingress_cidr_blocks" {
    type = list(string)
    default = ["10.10.0.0/16"]
}

variable "ingress_rules" {
  type = list(string)
  default = ["https-443-tcp"]
}

