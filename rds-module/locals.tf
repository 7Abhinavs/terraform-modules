locals {
  sg-id = data.terraform_remote_state.ecs-sg.outputs.ecs-sg-id
}
locals {
  private-subnet-id = data.terraform_remote_state.vpc.outputs.private-subnets
}