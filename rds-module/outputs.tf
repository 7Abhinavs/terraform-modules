output "rds-enpoint" {
  value = module.db.db_instance_endpoint
}

output "rds-address" {
  value = module.db.db_instance_address
}

output "rds-arn" {
  value = module.db.db_instance_arn
}