output "ecs-cluster-id" {
  value = aws_ecs_cluster.foo.id
}

output "ecs-task-definition-arn" {
  value = aws_ecs_task_definition.service.arn
}

