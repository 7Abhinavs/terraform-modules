
data "terraform_remote_state" "ecr" {
  backend = "local"
  config = {
    path = "../ecr-module/terraform.tfstate"
  }
} 

data "terraform_remote_state" "ecs-execution-role-terraform" {
  backend = "local"
  config = {
    path = "../iam-roles-module/terraform.tfstate"
  }
} 

data "template_file" "myapp-task-definition-template" {
  template = file("../templates/container.json.tpl")
  vars = {
    REPOSITORY_URL = replace(local.ecr-url, "https://", "")
  }
}

resource "aws_ecs_task_definition" "service" {
  family = "task"
  requires_compatibilities = ["FARGATE"]
  cpu = 512
  memory = 1024
  # operating_system_family = "LINUX"
  container_definitions = data.template_file.myapp-task-definition-template.rendered
  task_role_arn = local.ecs-execution-role-arn
  execution_role_arn = local.ecs-execution-role-arn
  network_mode             = "awsvpc"
}