
data "terraform_remote_state" "ecs-service-role-terraform" {
  backend = "local"
  config = {
    path = "../iam-roles-module/terraform.tfstate"
  }
} 

data "terraform_remote_state" "alb" {
  backend = "local"
  config = {
    path = "../alb-module/terraform.tfstate"
  }
} 

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../vpc-module/terraform.tfstate"
  }
} 

data "terraform_remote_state" "ecs-sg" {
  backend = "local"
  config = {
    path = "../security-group-module/terraform.tfstate"
  }
} 


resource "aws_ecs_service" "service" {
  name            = "service"
  cluster         = aws_ecs_cluster.foo.id
  task_definition = aws_ecs_task_definition.service.arn
  desired_count   = 1
  # iam_role        = local.ecs-service-arn
  # depends_on      = [aws_iam_role_policy_attachment.test-attach1]
   network_configuration {

    subnets = local.public-subnets
    security_groups = [local.sg-id]
    assign_public_ip = false
  }

  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }

  load_balancer {
    target_group_arn = local.tg-arn[0]
    container_name   = "myapp"
    container_port   = 3000
  }
}