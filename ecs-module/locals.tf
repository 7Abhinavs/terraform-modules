locals {
  ecr-url = data.terraform_remote_state.ecr.outputs.ecr
}

locals {
  ecs-execution-role-arn = data.terraform_remote_state.ecs-execution-role-terraform.outputs.ecs-execution-role-arn
}

locals {
  ecs-service-arn = data.terraform_remote_state.ecs-service-role-terraform.outputs.ecs-service-name
}

locals {
  alb-arn = data.terraform_remote_state.alb.outputs.alb-arn
}

locals {
  tg-arn = data.terraform_remote_state.alb.outputs.tg-arn
}

locals {
  public-subnets = data.terraform_remote_state.vpc.outputs.public_subnets-1
}

locals {
  sg-id = data.terraform_remote_state.ecs-sg.outputs.ecs-sg-id
}