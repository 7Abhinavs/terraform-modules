locals {
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id
}

locals {
  public_subnets = data.terraform_remote_state.vpc.outputs.public_subnets-1
}

locals {
  ecs-sg-id = data.terraform_remote_state.ecs-sg.outputs.ecs-sg-id
}