variable "alb-name" {
  type = string
  default = "my-alb"
}

variable "alb-type" {
  type = string
  default = "application"
}

# variable "tg-name" {
#   type = list(string)
#   default = "ecs-lb"
# }

# variable "backend_port" {
#   default = 80
# }

# variable "backend_protocol" {
#   default = "http"
# }

# variable "target_type" {
#   default = "instance"
# }

