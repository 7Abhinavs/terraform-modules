data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../vpc-module/terraform.tfstate"
  }
}

data "terraform_remote_state" "ecs-sg" {
  backend = "local"
  config = {
    path = "../security-group-module/terraform.tfstate"
  }
} 

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 8.0"

  name = var.alb-name

  load_balancer_type = var.alb-type

  vpc_id             = local.vpc_id
  subnets            = local.public_subnets
  security_groups    = [local.ecs-sg-id]

  target_groups = [
    {
    name = "ecs-lb"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "ip"
    } 
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
      type = "forward"
    
    }
    
  ]
  tags = {
    Environment = "Test"
  }
}
