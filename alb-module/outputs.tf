output "alb-arn" {
  value = module.alb.lb_arn
}

output "tg-arn" {
  value = module.alb.target_group_arns
}

