output "ecs-execution-role-name" {
  value = aws_iam_role.ecs-execution-role-terraform.name
}

output "ecs-execution-role-arn" {
  value = aws_iam_role.ecs-execution-role-terraform.arn
}

output "ecs-execution-role-id" {
  value = aws_iam_role.ecs-execution-role-terraform.id
}

output "ecs-service-name" {
  value = aws_iam_role.ecs-service-role-terraform.name
}

output "ecs-service-arn" {
  value = aws_iam_role.ecs-service-role-terraform.arn
}

output "ecs-service-id" {
  value = aws_iam_role.ecs-service-role-terraform.id
}
